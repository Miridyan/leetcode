/// https://leetcode.com/problems/reverse-integer/
/// Given a 32-bit signed integer, reverse digits of an integer.
///
/// Example 1:
/// 	Input: 123
/// 	Output: 321
///
/// Example 2:
/// 	Input: -123
/// 	Output: -321
///
/// Example 3:
/// 	Input: 120
/// 	Output: 21
///
///
/// Note:
/// Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−2^31,  2^(31 − 1)]. 
/// For the purpose of this problem, assume that your function returns 0 when the reversed integer overflows.

struct Solution;

impl Solution {
    pub fn reverse(x: i32) -> i32 {
        let mut res = 0i32;
        let mut x = x;

        while x != 0 {
            match res.checked_mul(10i32) {
                Some(i) => match i.checked_add(x % 10) {
                    Some(j) => res = j,
                    None	=> return 0,
                },
                None	=> return 0,
            }
            x /= 10;
        }
        res
    }
}


fn main() {
    println!("123 -> {}", Solution::reverse(123));
    println!("-123 -> {}", Solution::reverse(-123));
    println!("120 -> {}", Solution::reverse(120));
    println!("1056389759 -> {}", Solution::reverse(1056389759));
}