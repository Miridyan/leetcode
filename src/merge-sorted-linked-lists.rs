/// https://leetcode.com/problems/merge-two-sorted-lists/
///
/// Merge two sorted linked lists and return it as a new list. The new list should be made by splicing together the nodes of the first two lists.
///
/// Example:
///     Input: 1->2->4, 1->3->4
///     Output: 1->1->2->3->4->4


// Definition for singly-linked list.
#[derive(PartialEq, Eq, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        ListNode {
            next: None,
            val
        }
    }
}


struct Solution;

impl Solution {
    pub fn merge_two_lists(l1: Option<Box<ListNode>>, l2: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
        let mut l1_t = &l1 as *const Option<Box<ListNode>>;
        let mut l2_t = &l2 as *const Option<Box<ListNode>>;
        
        let mut list : Option<Box<ListNode>> = None;

        {
            let list_ptr = &mut list as *mut Option<Box<ListNode>>;
            
            'merge: loop {
                unsafe {
                    match (l1_t.as_ref()?, l2_t.as_ref()?) {
                        (None, None) => break 'merge,
                        (Some(ele1), None) => {
                            let tmp = Box::new(ListNode::new(ele1.val));

                            // unsafe {
                            // (*list_ptr)?.next = Some(tmp);
                            // (*list_ptr).take()?.next = Some(tmp);
                            // l1_t = &(*l1_t).take()?.next as *const Option<Box<ListNode>>;

                            // list_ptr = (*list_ptr)?.next;
                            // }
                        },
                        (None, Some(ele2)) => {
                            // let mut tmp = Box::new(ListNode::new(ele2.val));
                            // list_ptr.as_mut()?.next = Some(tmp);

                            // l2_t = &l2_t.as_ref()?.next;
                        },
                        (Some(ele1), Some(ele2)) => {
                            // let mut tmp1 = Box::new(ListNode::new(ele1.val));
                            // let mut tmp2 = Box::new(ListNode::new(ele2.val));

                            // tmp1.next = Some(tmp2);
                            // list_ptr.as_mut()?.next = Some(tmp1);

                            // l1_t = &l1_t.as_ref()?.next;
                            // l2_t = &l2_t.as_ref()?.next;
                        }
                    }

                }
            }
        }
        list
    }
}


fn main() {
    println!("Okay");
}